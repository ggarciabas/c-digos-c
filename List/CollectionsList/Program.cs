﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsList
{
    class Program
    {
        static void Main(string[] args)
        {
            IList<string> pessoas = new List<string>();
            pessoas.Add("Cassiano");
            pessoas.Add("Giovanna");
            pessoas.Add("Gian");
            Console.WriteLine("Lista completa:");
            foreach (var pessoa in pessoas)
            {
                Console.WriteLine(pessoa);
            }
            Console.WriteLine("\nBuscando uma pessoa:");
            for (int i = 0; i < 3; i++)
            {
                if (pessoas[i]=="Cassiano")
                {
                    Console.WriteLine(pessoas[i]);
                }
            }
            pessoas.RemoveAt(2);
            Console.WriteLine("\nApos a remocao de um elemento:");
            foreach (var pessoa in pessoas)
            {
                Console.WriteLine(pessoa);
            }
            Console.ReadKey();
        }
    }
}
