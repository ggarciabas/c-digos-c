﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsSortedList
{
    class Program
    {
        static void Main(string[] args)
        {
            SortedList<int,string> carros = new SortedList<int, string>();
            carros.Add(4,"fusca");
            carros.Add(3, "corcel");
            carros.Add(1, "maverick");
            carros.Add(2, "mustang");
            
            Console.WriteLine("Lista de carros ordenada:");
            foreach (var carro in carros)
            {
                Console.WriteLine("{0},{1}",carro.Key,carro.Value);
            }
            Console.WriteLine("Buscar carro no indice 2: "+carros.IndexOfKey(1));
            Console.ReadKey();
        }
    }
}
