﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsProjects
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<int,string > animais = new Dictionary<int, string>();
            animais.Add(1, "cachorro");
            animais.Add(2, "gato");
            animais.Add(3, "curio");
            animais.Add(4, "aranha");

            Console.WriteLine("Lista de animais:");
            foreach (KeyValuePair<int, string> par in animais)
            {
                    Console.WriteLine("{0}, {1}", par.Key, par.Value);
            }

            if (animais.ContainsValue("cachorro"))
            {
                Console.WriteLine("Animal encontrado.");
            }
            else
            {
                Console.WriteLine("Animal nao encontrado.");
            }

            if (animais.ContainsKey(5))
            {
                Console.WriteLine("Contem a chave.");
            }
            else
            {
                Console.WriteLine("Nao contem a chave.");
            }
            Console.ReadKey();
        }
    }
}
