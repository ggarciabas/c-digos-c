﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasseAbstrata
{
    public abstract class Animal
    {
        public string Nome { get; set; }
        public abstract string EmitirSom();
        public override string ToString()
        {
            return Nome+": "+EmitirSom();
        }
    }
}