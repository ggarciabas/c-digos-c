﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClasseAbstrata
{
    public class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animais = new List<Animal>();
            animais.Add(new Cachorro { Nome = "Bob" });
            animais.Add(new Gato { Nome = "Frajola" });
            animais.Add(new Cachorro { Nome = "Tob" });
            animais.Add(new Gato { Nome = "Tom" });
            animais.Add(new Cachorro { Nome = "Cherry" });
            animais.Add(new Pintinho { Nome = "PiuPiu" });

            foreach (Animal item in animais)
            {
                Console.WriteLine(item);
            }

            Console.ReadKey();
        }
    }
}
