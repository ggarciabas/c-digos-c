﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsQueue
{
    class Program
    {
        static void Main(string[] args)
        {
            Queue<string> plantas = new Queue<string>();
            plantas.Enqueue("rosa");
            plantas.Enqueue("margarida");
            plantas.Enqueue("violeta");
            plantas.Enqueue("alcachofra");

            Console.WriteLine("Retorna mas não remove: "+plantas.Peek());
            Console.WriteLine("Retorna removendo da fila: " + plantas.Dequeue());
            Console.WriteLine("Retorna removendo da fila: " + plantas.Dequeue());
            Console.WriteLine("Itens restantes da fila:");
            foreach (var planta in plantas)
            {
                Console.WriteLine(planta);
            }
            Console.ReadKey();
        }
    }
}
