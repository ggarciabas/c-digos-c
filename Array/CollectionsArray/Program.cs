﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsArray
{
    class Program
    {
        
        static void Main(string[] args)
        {
            
            String[] listaNomes = new string[] {"cassiano","gian","giovanna","alguem"};
            Console.WriteLine("Imprime elemento com indice 2 no vetor: " + listaNomes[2]);
            Console.WriteLine("\nElementos do vetor:");
            for (int i = 0; i < listaNomes.Length; i++)
            {
                Console.WriteLine(listaNomes[i]);
            }
            Console.ReadKey();
        }
    }
}
