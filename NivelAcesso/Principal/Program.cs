﻿using NivelAcesso_1;
using NivelAcesso_2;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Principal
{
    class Program
    {
        static void Main(string[] args)
        {
            ClasseNivelAcesso cna = new ClasseNivelAcesso();
            cna.ValidarNivelAcesso();

            ClasseHerancaDentroAssembly chda = new ClasseHerancaDentroAssembly();
            chda.ValidarNivelAcesso();

            ClasseDentroAssembly cda = new ClasseDentroAssembly();
            cda.ValidarNivelAcesso(cna);

            ClasseHerancaForaAssembly chfa = new ClasseHerancaForaAssembly();
            chfa.ValidarNivelAcesso();

            ClasseForaAssembly cfa = new ClasseForaAssembly();
            cfa.ValidarNivelAcesso(cna);

            Console.ReadKey();
        }
    }
}
