﻿using NivelAcesso_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NivelAcesso_2
{
    public class ClasseForaAssembly
    {
        public void ValidarNivelAcesso(ClasseNivelAcesso cna)
        {
            Console.WriteLine("\n\nEu sou a ClasseForaAssembly e tenho permissão para executar os seguintes métodos: ");

            // cna.MetodoPrivate(); 
            // cna.MetodoProtected();
            // cna.MetodoInternal();
            // cna.MetodoProtectedInternal();
            cna.MetodoPublic();
        }
    }
}
