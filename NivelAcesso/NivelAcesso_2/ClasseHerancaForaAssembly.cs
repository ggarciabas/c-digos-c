﻿using NivelAcesso_1;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NivelAcesso_2
{
    public class ClasseHerancaForaAssembly : ClasseNivelAcesso
    {
        public void ValidarNivelAcesso() {
            Console.WriteLine("\n\nEu sou a ClasseHerancaForaAssembly e tenho permissão para executar os seguintes métodos: ");

            // MetodoPrivate(); 
            MetodoProtected();
            // MetodoInternal();
            MetodoProtectedInternal();
            MetodoPublic();
        }
    }
}
