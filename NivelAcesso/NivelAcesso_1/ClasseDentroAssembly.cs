﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NivelAcesso_1
{
    public class ClasseDentroAssembly
    {
        public void ValidarNivelAcesso (ClasseNivelAcesso cna)
        {
            Console.WriteLine("\n\nEu sou a ClasseDentroAssembly e tenho permissão para executar os seguintes métodos: ");

            //cna.MetodoPrivate(); 
            //cna.MetodoProtected();
            cna.MetodoInternal();
            cna.MetodoProtectedInternal();
            cna.MetodoPublic();
        }
    }
}
