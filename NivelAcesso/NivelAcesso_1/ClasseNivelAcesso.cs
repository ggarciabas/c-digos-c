﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NivelAcesso_1
{
    public class ClasseNivelAcesso
    {
        public void ValidarNivelAcesso() {
            Console.WriteLine("\nEu sou a ClasseNivelAcesso e tenho permissão para executar os seguintes métodos: ");
            MetodoPrivate();
            MetodoProtected();
            MetodoInternal();
            MetodoProtectedInternal();
            MetodoPublic();
        }

        private void MetodoPrivate()
        {
            Console.WriteLine("Este método tem como nível de acesso private.");
        }

        protected void MetodoProtected()
        {
            Console.WriteLine("Este método tem como nível de acesso protected.");
        }

        internal void MetodoInternal()
        {
            Console.WriteLine("Este método tem como nível de acesso internal.");
        }

        protected internal void MetodoProtectedInternal()
        {
            Console.WriteLine("Este método tem como nível de acesso protected internal.");
        }

        public void MetodoPublic()
        {
            Console.WriteLine("Este método tem como nível de acesso public.");
        }
    }
}
