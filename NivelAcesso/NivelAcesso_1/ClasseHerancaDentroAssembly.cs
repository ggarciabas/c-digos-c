﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NivelAcesso_1
{
    public class ClasseHerancaDentroAssembly : ClasseNivelAcesso
    {
        public void ValidarNivelAcesso() {
            Console.WriteLine("\n\nEu sou a ClasseHerancaDentroAssembly e tenho permissão para executar os seguintes métodos: ");

            //MetodoPrivate(); 
            MetodoProtected();
            MetodoInternal();
            MetodoProtectedInternal();
            MetodoPublic();
        }
    }
}
