﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsTipadoNaoTipado
{
    class Program
    {
        static void Main(string[] args)
        {
            List<object> generico = new List<object>();
            generico.Add(1);//tipo integer
            generico.Add("texto");//tipo string
            generico.Add(1.2);//tipo double
            foreach (var o in generico)
            {
                if (o is int)
                {
                    Console.WriteLine("Valor int: " + o);
                }
                else if(o is string)
                {
                    Console.WriteLine("Valor string: " + o);
                }
                else
                {
                    Console.WriteLine("Valor double: " + o);
                }
            }
            Console.ReadKey();
        }
    }
}
