﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public class Loteria: ISorteavel
    {
        //propriedades
        public int[] Numeros { get; set; }

        //indexador
        public int this[int i]
        {
            get { return Numeros[i]; }
            set { Numeros[i] = value;}
        }

        public Loteria() {
            Numeros = new int[10];
        }

        //metodos
        public void sortear() {
            Random random = new Random();
            for (int i = 0; i < 10; i++)
            {
                Numeros[i] = (random.Next()%100);
            }
        }
    }
}
