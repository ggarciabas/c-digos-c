﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public class Program
    {
        static void Main(string[] args)
        {
            ISorteavel loteria = new Loteria();

            loteria.sortear();

            foreach (int item in loteria.Numeros)
            {
                Console.Write(item+" ");
            }
            Console.WriteLine("\n");

            for (int i = 0; i < loteria.Numeros.Length; i++)
			{
                Console.WriteLine((i+1)+"º: "+loteria[i]);
			}
            Console.ReadKey();
        }
    }
}
