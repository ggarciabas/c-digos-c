﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Interface
{
    public interface ISorteavel
    {
        //propriedades
        int [] Numeros {get; set;}

        //indexador
        int this[int i]
        {
            get;
            set;
        }

        //metodos
        void sortear();
    }
}
