﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Alarme alarme = new Alarme();
            Guarda guarda = new Guarda(alarme);
            
            alarme.Dispara();
            
            Console.ReadKey();
        }

    }
}
