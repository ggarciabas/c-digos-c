﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event
{
    public class Guarda
    {
        public Alarme Alarme { get; set; }
        public Guarda(Alarme alarme)
        {
            this.Alarme = alarme;
            alarme.Disparado += AlarmeDisparado;
            alarme.Desligado += AlarmeDesligado;
        }
        public void AlarmeDesligado(string aviso)
        {
            Console.WriteLine(aviso);
        }
        public void AlarmeDisparado(string aviso)
        {
            Console.WriteLine(aviso);
            Console.WriteLine("Guarda recebe a notificação que o alarme foi disparado e desliga o alarme!");
            Alarme.Desliga();
        }
    }
}