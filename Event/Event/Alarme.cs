﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Event
{
    public delegate void StringEventHandler(String aviso);
    public class Alarme
    {
        public event StringEventHandler Disparado;
        public event StringEventHandler Desligado;
        public bool Tocando { get; set; }
        public Alarme()
        {
            Tocando = false;
        }
        public void Dispara()
        {
            this.Tocando = true;
            if (Disparado != null)
            {
                Disparado("O Alarme foi Disparado!!!");
            }
        }
        public void Desliga()
        {
            this.Tocando = false;
            if (Desligado != null)
            {
                Desligado("O Alarme foi Desligado.");
            }
        }
    }
}
