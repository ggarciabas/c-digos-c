﻿using Propriedade;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Principal
{
    class Program
    {
        static void Main(string[] args)
        {
            ClassePropriedade cp = new ClassePropriedade()
            {
                Propriedade_2 = 12
            };

            Console.WriteLine("Apresentando Propriedade_1: " + cp.Propriedade_1);
            Console.WriteLine("Apresentando Propriedade_2: " + cp.Propriedade_2);

            Console.ReadKey();
        }
    }
}



