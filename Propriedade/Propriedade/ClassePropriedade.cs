﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Propriedade
{
    public class ClassePropriedade
    {
        public ClassePropriedade() {
            Propriedade_1 = 1;
        }

        // Somente leitura
        public int Propriedade_1 { get; private set; }
        public int propriedade_2;
        public int Propriedade_2
        {
            get
            {
                return propriedade_2;
            }
            set
            {                
                propriedade_2 = value * 2;
            }
        }
    }
}
