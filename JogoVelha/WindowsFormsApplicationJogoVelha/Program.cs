﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplicationJogoVelha
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);

            JogoVelhaModel Model = new JogoVelhaModel();
            FormJogoVelha View = new FormJogoVelha(Model);
            Controller Controller = new Controller(View, Model);

            Application.Run(View);
        }
    }
}
