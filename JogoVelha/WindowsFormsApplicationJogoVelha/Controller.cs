﻿
namespace WindowsFormsApplicationJogoVelha
{
  public class Controller
    {
      public FormJogoVelha View { get; private set; }
      public JogoVelhaModel Model { get; private set; }

      public Controller(FormJogoVelha view, JogoVelhaModel model) {
          this.View = view;
          this.Model = model;
          this.View.NovaJogadaSolicitada += View_NovaJogadaSolicitada;
          this.View.NovoJogoSolicitado += View_NovoJogoSolicitado;
      }

      void View_NovoJogoSolicitado()
      {
          Model.NovoJogo();
      }

      void View_NovaJogadaSolicitada(int a, int b)
      {
          Model.Jogada(a, b);
      }
  }
}
