﻿using System;
using System.Windows.Forms;

namespace WindowsFormsApplicationJogoVelha
{
    public partial class FormJogoVelha : Form
    {
        public JogoVelhaModel Model{get; private set;}

        public event VazioEventHandler NovoJogoSolicitado;
        public event IntIntEventHandler NovaJogadaSolicitada;

        public FormJogoVelha(JogoVelhaModel model)
        {
            InitializeComponent();
            this.Model = model;
            this.Model.JogoEncerrado += Model_JogoEncerrado;
            this.Model.JogoAtualizado += Model_JogoAtualizado;
        }

        void Model_JogoEncerrado()
        {
            MessageBox.Show("Para iniciar um novo jogo clique em 'Novo Jogo'", "Jogo Encerrado");
        }

        void Model_JogoAtualizado()
        {
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Button button = tableLayoutPanel1.GetControlFromPosition(i, j) as Button;
                    button.Text = Model.Matriz[i, j].ToString();
                }
            }
            if (Model.Estado == JogoVelhaModel.X_VENCEU)
            {
                MessageBox.Show("Jogador X venceu!", "Parabéns!");
            }
            else if (Model.Estado == JogoVelhaModel.O_VENCEU)
            {
                MessageBox.Show("Jogador O venceu!", "Parabéns!");
            }
            else if (Model.Estado == JogoVelhaModel.VELHA)
            {
                MessageBox.Show("Deu Velha!", "Own!");
            }
        }

       private void novoJogoToolStripMenuItem_Click(object sender, EventArgs e)
       {
           if (NovoJogoSolicitado != null)
               NovoJogoSolicitado();
       }

       private void button9_Click(object sender, EventArgs e)
       {
           if (NovaJogadaSolicitada != null) {
               NovaJogadaSolicitada(
                   tableLayoutPanel1.GetCellPosition((Control)sender).Column,
                   tableLayoutPanel1.GetCellPosition((Control)sender).Row);
               
           }
       }
    }
}
