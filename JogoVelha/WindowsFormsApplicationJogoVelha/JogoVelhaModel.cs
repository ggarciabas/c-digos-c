﻿
namespace WindowsFormsApplicationJogoVelha
{
    public class JogoVelhaModel
    {
        public char[,] Matriz { get; private set; }
        public bool XVez { get; private set; }
        public int Estado { get; private set; }
        public int Jogadas { get; private set; }

        public const int EM_JOGO = 0;
        public const int X_VENCEU = 1;
        public const int O_VENCEU = 2;
        public const int VELHA = 3;

        public event VazioEventHandler JogoAtualizado;
        public event VazioEventHandler JogoEncerrado;

        public JogoVelhaModel()
        {
            NovoJogo();
        }

        public void NovoJogo()
        {
            Matriz = new char[3, 3];
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    Matriz[i, j] = ' ';
                }
            }
            XVez = true;
            Estado = EM_JOGO;
            Jogadas = 0;
            if (JogoAtualizado != null)
                JogoAtualizado();
        }

        public void Jogada(int x, int y)
        {
            if (Estado != EM_JOGO)
            {
                if (JogoEncerrado != null)
                    JogoEncerrado();
            }
            else if (!(Matriz[x, y] != ' ' || x < 0 || x > 2 || y < 0 || y > 2))
            {
                if (XVez)
                    Matriz[x, y] = 'x';
                else
                    Matriz[x, y] = 'o';

                XVez = !XVez;
                Jogadas++;
                Avalia();

                if (JogoAtualizado != null)
                    JogoAtualizado();
            }
        }
        

        public void Avalia()
        {
            if ((Matriz[0, 0] == 'x' && Matriz[0, 1] == 'x' && Matriz[0, 2] == 'x')
             || (Matriz[1, 0] == 'x' && Matriz[1, 1] == 'x' && Matriz[1, 2] == 'x')
             || (Matriz[2, 0] == 'x' && Matriz[2, 1] == 'x' && Matriz[2, 2] == 'x')
             || (Matriz[0, 0] == 'x' && Matriz[1, 0] == 'x' && Matriz[2, 0] == 'x')
             || (Matriz[0, 1] == 'x' && Matriz[1, 1] == 'x' && Matriz[2, 1] == 'x')
             || (Matriz[0, 2] == 'x' && Matriz[1, 2] == 'x' && Matriz[2, 2] == 'x')
             || (Matriz[0, 0] == 'x' && Matriz[1, 1] == 'x' && Matriz[2, 2] == 'x')
             || (Matriz[0, 2] == 'x' && Matriz[1, 1] == 'x' && Matriz[2, 0] == 'x'))
            {
                Estado = X_VENCEU;
            }
            else if ((Matriz[0, 0] == 'o' && Matriz[0, 1] == 'o' && Matriz[0, 2] == 'o')
            || (Matriz[1, 0] == 'o' && Matriz[1, 1] == 'o' && Matriz[1, 2] == 'o')
            || (Matriz[2, 0] == 'o' && Matriz[2, 1] == 'o' && Matriz[2, 2] == 'o')
            || (Matriz[0, 0] == 'o' && Matriz[1, 0] == 'o' && Matriz[2, 0] == 'o')
            || (Matriz[0, 1] == 'o' && Matriz[1, 1] == 'o' && Matriz[2, 1] == 'o')
            || (Matriz[0, 2] == 'o' && Matriz[1, 2] == 'o' && Matriz[2, 2] == 'o')
            || (Matriz[0, 0] == 'o' && Matriz[1, 1] == 'o' && Matriz[2, 2] == 'o')
            || (Matriz[0, 2] == 'o' && Matriz[1, 1] == 'o' && Matriz[2, 0] == 'o'))
            {
                Estado = O_VENCEU;
            }
            else if (Jogadas == 9)
            {
                Estado = VELHA;
            }
        }
    }
}
