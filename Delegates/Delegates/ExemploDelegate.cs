﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Delegates
{
    delegate double OperacaoMatematica(double num);
    
    public class ExemploDelegate
    {
        static double Dobro(double entrada)
        {
            return entrada * 2;
        }
        static void Main()
        {
            //inicialização por método declarado
            OperacaoMatematica om = Dobro;
            double multiplicaPorDois = om(3.5);
            Console.WriteLine("3.5 multiplicado por 2: {0}", multiplicaPorDois);
            
            //inicialização por método anônimo
            OperacaoMatematica om2 = delegate(double entrada)
            {
                return entrada * entrada;
            };
            double quadrado = om2(6);
            Console.WriteLine("Quadrado de 6: {0}", quadrado);
            
            //inicialização por expressão Lambda
            OperacaoMatematica om3 = entrada => entrada * entrada * entrada;
            double cubo = om3(6);
            Console.WriteLine("Cubo de 6: {0}", cubo);
           
            
            Console.ReadKey();
        }
    }

}
