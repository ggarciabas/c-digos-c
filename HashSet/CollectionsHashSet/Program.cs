﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsHashSet
{
    class Program
    {
        static void Main(string[] args)
        {
            ISet<string> pessoas = new HashSet<string>();
            pessoas.Add("cassiano");
            pessoas.Add("giovanna");
            pessoas.Add("gian");
            pessoas.Add("paulo");
            pessoas.Add("cassiano");
            pessoas.Add("giovanna");
            pessoas.Add("gian");

            

            foreach (var pessoa in pessoas)
            {
                Console.WriteLine(pessoa);
            }
            Console.ReadKey();

        }
    }
}
