﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CollectionsStack
{
    class Program
    {
        static void Main(string[] args)
        {
            Stack<string> pratos = new Stack<string>();
            pratos.Push("gigante");
            pratos.Push("grande");
            pratos.Push("medio");
            pratos.Push("pequeno");
            pratos.Push("miniatura");

            Console.WriteLine("Pilha completa:");

            foreach (var prato in pratos)
            {
                Console.WriteLine(prato);
            }

            string mostraPrato = pratos.Peek();
            Console.WriteLine("\nBuscado mas nao retirado: " + mostraPrato);
           
            Console.WriteLine("\nRemovendo: " + pratos.Pop());
            Console.WriteLine("Removendo: " + pratos.Pop());
            Console.WriteLine("Removendo: " + pratos.Pop());

            Console.WriteLine("\nPilha apos a remocao:");

            foreach (var prato in pratos)
            {
                Console.WriteLine(prato);
            }
            Console.ReadKey();
        }
    }
}
