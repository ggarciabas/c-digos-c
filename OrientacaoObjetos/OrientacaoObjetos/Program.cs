﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrientacaoObjetos
{

   public class Program
    {
       public static void MensagemBoasVindas(Pessoa p) { 
            if(p is Aluno)
                Console.WriteLine("Olá Aluno: "+ (p as Aluno));
            else
                Console.WriteLine("Olá: "+ p);
       }

        static void Main(string[] args)
        {
            Pessoa p = new Pessoa("012.654.658-96", "Adamastor Joaquim", "adamastor@email.com");
            Aluno a = new Aluno("122.853.556-56", "João da Silva", "joaozinho@email.com", 911213);
        
            MensagemBoasVindas(p);
            MensagemBoasVindas(a);

            Console.ReadKey();
        }
    }
}
