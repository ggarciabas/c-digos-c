﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrientacaoObjetos
{
    public class Pessoa
    {
        //campos
        private string email;
        //propriedades
        public string CPF { get; set; }
        public string Nome { get; set; }
        public string Email { get{return email;} 
            set{
                if (ValidaEmail(value))
                    this.email = value;
            } 
        }
        //método construtor
        public Pessoa(string cpf, string nome, string email)
        {
            this.CPF = cpf;
            this.Nome = nome;
            this.Email = email;
        }
        //Outros métodos
        public bool ValidaEmail(string email /*parâmetros*/)
        {
            if (email.Contains("@"))
                return true;
            return false;
        }
        //sobrecarga do construtor Pessoa
        public Pessoa(string cpf, string nome)
        {
            this.CPF = cpf;
            this.Nome = nome;
            this.Email = "";
        }
        //sobrescrita do método ToString
        public override string ToString()
        {
            return Nome + "\n\tCPF: " + CPF + "\n\tE-mail: " + Email;
        }

    }
}
