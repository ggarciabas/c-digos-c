﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrientacaoObjetos
{
    public class Aluno : Pessoa
    {
        public int RA { get; set; }
        public Aluno(string cpf, string nome, string email, int ra)
            : base(cpf, nome, email)
        {
            this.RA = ra;
        }
        //sobrescrita do método ToString
        public override string ToString()
        {
            return base.ToString() + "\n\tRa: " + RA;
        }
    }
}
