﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exceptions
{
    class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Calcula divisão de dois números");
            try
            {
                Console.WriteLine("Informe o Dividendo (Número inteiro)");
                int a = Int32.Parse(Console.ReadLine());
                Console.WriteLine("Informe o Divisor (Número inteiro)");
                int b = Int32.Parse(Console.ReadLine());
                if (b == 0)
                    throw new DivideByZeroException();
                Console.WriteLine
                    ("O Quociente de {0} dividido por {1} é {2}", a, b, (float)a / b);
            }
            catch (FormatException formatException)
            {

                Console.WriteLine("Erro: {0}", formatException.Message);
            }
            catch (DivideByZeroException divideByZeroException)
            {
                Console.WriteLine("Erro: {0}", divideByZeroException.Message);
            }
            finally
            {
                Console.ReadKey();
            }
        }
    }
}
